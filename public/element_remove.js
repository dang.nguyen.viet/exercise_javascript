function removeElement() {
  element = document.getElementById('element');
  element.remove();
}

function getElementRemovePage() {
  div_tag = document.createElement('div');
  h1_tag = document.createElement('h1');
  h1_tag.innerHTML = 'đây là 1 element';
  h1_tag.id = 'element';
  div_tag.appendChild(h1_tag);
  button_tag = document.createElement('button');
  button_tag.innerHTML = 'Delete Element';
  button_tag.onclick = () => removeElement();
  div_tag.appendChild(button_tag);
  return div_tag;
}
