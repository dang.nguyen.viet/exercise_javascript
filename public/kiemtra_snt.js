function isPrime(number) {
  if (number < 2) return false;
  if (number == 2) return true;
  for (let index = 2; index <= number / 2; index++) {
    if (number % index == 0) return false;
  }
  return true;
}

function showResult() {
  let number = document.getElementById('number_input').value;
  if (number == '') {
    document.getElementById('prime').innerHTML = 'Hay nhap vao 1 so';
  } else {
    if (isPrime(number)) {
      document.getElementById('prime').innerHTML = number + ' la so nguyen to';
    } else {
      document.getElementById('prime').innerHTML =
        number + ' khong phai la so nguyen to';
    }
  }
}

function getCheckPrimePage() {
  div_tag = document.createElement('div');

  input_tag = document.createElement('input');
  input_tag.type = 'text';
  input_tag.placeholder = 'Nhập vào 1 số';
  input_tag.id = 'number_input';
  div_tag.appendChild(input_tag);

  button_tag = document.createElement('button');
  button_tag.innerHTML = 'Check';
  button_tag.onclick = () => showResult();
  div_tag.appendChild(button_tag);

  p_tag = document.createElement('p');
  p_tag.id = 'prime';
  div_tag.appendChild(p_tag);

  return div_tag;
}
