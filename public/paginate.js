let list_pages = getAllPage();

let number_of_item = list_pages.length;
let item_per_page = 1;
let number_of_page = number_of_item / item_per_page;

div_wrapper = document.getElementById('wrapper');
div_wrapper.style.position = 'fixed';
div_wrapper.style.bottom = '100px';

paginate_bar = document.getElementById('paginate-bar');

button_tag1 = document.createElement('button');
button_tag1.id = 'btn1';
button_tag1.innerHTML = 1;
button_tag1.onclick = () => changePage(Number(button_tag1.innerHTML) - 1);

left_arrow_button = document.createElement('button');
left_arrow_button.innerHTML = '<<';
left_arrow_button.onclick = () => changePaginateBar('left');
left_arrow_button.style.visibility = 'hidden';

button_tag2 = document.createElement('button');
button_tag2.id = 'btn2';
button_tag2.innerHTML = 2;
button_tag2.onclick = () => changePage(Number(button_tag2.innerHTML) - 1);

right_arrow_button = document.createElement('button');
right_arrow_button.innerHTML = '>>';
right_arrow_button.onclick = () => changePaginateBar('right');

paginate_bar.appendChild(left_arrow_button);
paginate_bar.appendChild(button_tag1);
paginate_bar.appendChild(button_tag2);
paginate_bar.appendChild(right_arrow_button);

function changePage(index) {
  page_content = document.getElementById('page-content');
  page_content.innerHTML = '';
  page_content.appendChild(list_pages[index]);
}

function changePaginateBar(position) {
  btn1 = document.getElementById('btn1');
  btn2 = document.getElementById('btn2');

  if (position == 'left') {
    btn1.innerHTML = Number(btn1.innerHTML) - 1;
    btn2.innerHTML = Number(btn2.innerHTML) - 1;
  }

  if (position == 'right') {
    btn1.innerHTML = Number(btn2.innerHTML);
    btn2.innerHTML = Number(btn2.innerHTML) + 1;
  }

  if (btn1.innerHTML == 1) {
    left_arrow_button.style.visibility = 'hidden';
  } else {
    left_arrow_button.style.visibility = 'visible';
  }

  if (btn2.innerHTML == number_of_item) {
    right_arrow_button.style.visibility = 'hidden';
  } else {
    right_arrow_button.style.visibility = 'visible';
  }
}

function getAllPage() {
  let list_pages = new Array();
  add_html_tag_page = getAddHtmlPage();
  div_show_hide_page = getDivShowHidePage();
  cal_sum_page = getCalculateSumPage();
  element_remove_page = getElementRemovePage();
  check_prime_page = getCheckPrimePage();
  draw_page = getDrawPage();
  list_pages.push(
    add_html_tag_page,
    div_show_hide_page,
    cal_sum_page,
    element_remove_page,
    check_prime_page,
    draw_page
  );
  return list_pages;
}
