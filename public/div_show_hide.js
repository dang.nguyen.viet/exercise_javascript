function getDivShowHidePage() {
  div_tag = document.createElement('div');

  div_tag_child = document.createElement('div');
  div_tag_child.id = 'div-tag';
  h1_tag = document.createElement('h1');
  h1_tag.innerHTML = 'Đây là thẻ div';
  div_tag_child.appendChild(h1_tag);
  div_tag.appendChild(div_tag_child);

  button_tag = document.createElement('button');
  button_tag.innerHTML = 'Show/Hide div';
  button_tag.onclick = () => divShowHide();

  div_tag.appendChild(button_tag);
  return div_tag;
}

function divShowHide() {
  hidden = document.getElementById('div-tag').hidden;
  document.getElementById('div-tag').hidden = !hidden;
}
