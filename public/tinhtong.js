var even_sum = 0;
var odd_sum = 0;
for (let i = 1; i <= 100; i++) {
  i % 2 == 0 ? (even_sum += i) : (odd_sum += i);
}

function getCalculateSumPage() {
  div_tag = document.createElement('div');
  h3_tag = document.createElement('h3');
  h3_tag.innerHTML = 'Tổng chẵn từ 1 -> 100: ' + even_sum;
  div_tag.appendChild(h3_tag);
  h3_tag = document.createElement('h3');
  h3_tag.innerHTML = 'Tổng lẻ từ 1 -> 100: ' + odd_sum;
  div_tag.appendChild(h3_tag);
  return div_tag;
}
