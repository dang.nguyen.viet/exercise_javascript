var content = '';

function veHinhTamGiac(num_of_line) {
  content += "<div style='position:absolute;left:512px;top:100px'>";
  for (let i = 0; i < num_of_line; i++) {
    for (let j = 0; j <= i; j++) {
      if (j > 0 && j < i && i < num_of_line - 1)
        content += "<span style='visibility:hidden' >*&nbsp&nbsp&nbsp</span>";
      else content += '<span>*&nbsp&nbsp&nbsp</span>';
    }
    content += '<br/><br/>';
  }
  content += '</div>';
}

function veHinhTamGiacRevert(num_of_line) {
  content += "<div style='position:absolute;left:512px;top:300px'>";
  for (let i = num_of_line - 1; i >= 0; i--) {
    for (let j = 0; j <= i; j++) {
      content += '*&nbsp&nbsp&nbsp';
    }
    content += '<br/><br/>';
  }
  content += '</div>';
}

function veHinhVuongRong(width, height) {
  content += "<div style='position:absolute;left:256px;top:100px'>";
  real_height = height * 2 - 1;
  for (let i = 0; i < real_height; i++) {
    if (i == 0 || i == real_height - 1) {
      for (let j = 0; j < width - 1; j++) {
        content += '*&nbsp&nbsp&nbsp';
      }
      content += '*<br/><br/>';
    } else if (i % 2 == 0) {
      content += '*&nbsp&nbsp&nbsp';
      for (let j = 0; j < width - 2; j++) {
        content += "<span style='visibility:hidden' >*&nbsp&nbsp&nbsp</span>";
      }
      content += '*<br/><br/>';
    }
  }
  content += '</div>';
}

function veHinhVuong(width, height) {
  content += "<div style='position:absolute;left:256px;top:300px'>";
  real_height = height * 2 - 1;
  for (let i = 0; i < real_height; i++) {
    if (i == 0 || i == real_height - 1) {
      for (let j = 0; j < width - 1; j++) {
        content += '*&nbsp&nbsp&nbsp';
      }
      content += '*<br/><br/>';
    } else if (i % 2 == 0) {
      content += '*&nbsp&nbsp&nbsp';
      for (let j = 0; j < width - 2; j++) {
        content += '<span>*&nbsp&nbsp&nbsp</span>';
      }
      content += '*<br/><br/>';
    }
  }
  content += '</div>';
}

function veTamGiacNguoc(width, height) {
  content += "<div style='position:absolute;left:64px;top:300px'>";
  for (let i = 0; i < height; i++) {
    for (let j = 0; j < width - i; j++) {
      if (j < i)
        content += "<span style='visibility:hidden' >*&nbsp&nbsp&nbsp</span>";
      else content += '<span>*&nbsp&nbsp&nbsp</span>';
    }
    content += '<br/><br/>';
  }
  content += '</div>';
}

function veTamGiacRong(width, height) {
  content += "<div style='position:absolute;left:64px;top:100px'>";
  for (let i = height - 1; i >= 0; i--) {
    for (let j = 0; j < width - i; j++) {
      if (j < i || (j > i && j < width - i - 1 && i > 0))
        content += "<span style='visibility:hidden' >*&nbsp&nbsp&nbsp</span>";
      else content += '<span>*&nbsp&nbsp&nbsp</span>';
    }
    content += '<br/><br/>';
  }
  content += '</div>';
}

veHinhTamGiac(4);
veHinhTamGiacRevert(4);
veHinhVuongRong(8, 4);
veHinhVuong(8, 4);

// điều kiện vẽ tam giác cân: width = height * 2 - 1
veTamGiacNguoc(7, 4);
veTamGiacRong(7, 4);

content = "<div style='position:fixed; left:320px'>" + content + '</div>';

function getDrawPage() {
  div_tag = document.createElement('div');
  div_tag.innerHTML = content;
  return div_tag;
}
