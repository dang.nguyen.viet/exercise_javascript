function getAddHtmlPage() {
  div_tag = document.createElement('div');
  div_tag.id = 'add-page-div';
  h1_tag = document.createElement('h1');
  h1_tag.innerHTML = 'bấm vào nút dưới để thêm 1 element';
  div_tag.appendChild(h1_tag);
  button_tag = document.createElement('button');
  button_tag.onclick = () => addElement();
  button_tag.innerHTML = 'Add element';
  div_tag.appendChild(button_tag);
  return div_tag;
}

function addElement() {
  console.log('click');
  div = document.getElementById('add-page-div');
  new_tag = document.createElement('h1');
  new_tag.innerHTML = 'Thẻ mới tạo';
  div.appendChild(new_tag);
}
